
# Installation
* Setup virtualenv:
```
python3 -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
```
* Run locally: `python server.py`

### Docker things
* Build: `docker build -t ipegress .`
* Run: ` docker run -e PORT=4999 -p 4999:4999 ipegress`