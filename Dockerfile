FROM python:3.10-slim-bullseye

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt
COPY server.py server.py
CMD [ "python", "./server.py" ]