import requests
import os
from flask import Flask, request, jsonify

app = Flask(__name__)
@app.route('/', methods=['GET'])
def getPublicEgressIP():
    url = 'https://ifconfig.me'
    r = requests.get(url)
    egressIP = None
    if r.status_code == 200:
        print("Egress is: ", r.text)
        egressIP = r.text
    return jsonify({'callingIP': request.remote_addr, 'egressIP': egressIP}), 200

if __name__ == '__main__':
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port)
